import requests as rq
import re

config = {
    'wiki_api_url': 'https://wiki.swarma.org/api.php?action=query&format=json&list=allpages&aplimit=500',
    'termlist_path': './wiki_entries.txt',
}

def term_filter(s):
    """Remove invalid terms"""
    if '-' in s:
        if not re.match('[a-z]', s) and not re.match('[一-龟]', s):
            return False
        # else: print(s, re.findall('[a-z]', s), re.findall('[一-龟]', s))
    return True
extract_titles = lambda data: [ x["title"] for x in data["query"]["allpages"] if term_filter(x["title"]) ]

## Get all page titles
data = rq.get(config['wiki_api_url']).json()
result = extract_titles(data)
while data.get("continue"): # paging
    result += extract_titles(data)
    data = rq.get(f'{config["wiki_api_url"]}&apcontinue={data["continue"]["apcontinue"]}').json()
## Print to txt file
with open(config['termlist_path'], "w", encoding='utf8') as f:
    for entry in result:
        f.write(entry + "\n")
