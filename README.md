## Setup

- [Deno](https://deno.land/#installation)
- Python dependencies

```bash
pip install -r requirements.txt
```

## Use

1. Get latest term list from wiki `python get_wiki_entries.py`
2. Start jieba server `python jieba-server.py`
3. Run deno script `deno run -A replace_html.deno.js`

## Example

- Input: https://www.dpaste.org/L0VTt
- Output: https://www.dpaste.org/Ehbao



## Workflow

```
get_wiki_entries.py       jieba-server.py      |    replace_html.deno.js
                                               |        ┌────────┐
                   |                           |        │raw.html│
                   |                           |        └────┬───┘
        ┌──────┐   |                           |             │
        │ wiki │   |                           |    ┌────────▼────────┐
        └───┬──┘   |          ┌─────────────────┐   │ parse .html DOM │
            │      |          │                |│   └────────┬────────┘
            │      |    ┌─────▼──────────────┐ |│            │
      ┌─────▼────┬─|────► string replacement │ |│   ┌────────▼─────────┐
      │terms.txt │ |    └─────────────▲─────┬┘ |└───┤ extract textNode │
      └──────────┤ |                  │     │  |    └──────────────────┘
                 │ | ┌──────────┐     │     │  |
                 └─|─► userdict │     │     │  |    ┌──────────────────┐
                   | └──────────┼─────┴───┐ └───────► update DOM nodes │
                   |            │jieba.cut│    |    └────────┬─────────┘
                   |    ┌───────┴─────────┤    |             │
                   |    │ text-processing │    |     ┌───────▼────────┐
                   |    │     server      │    |     │☆processed.html │
                   |    └─────────────────┘    |     └────────────────┘
```