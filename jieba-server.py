from functools import reduce

import requests as rq
from sanic import Sanic
from sanic import response
from sanic_cors import CORS
from  codetiming import Timer

app = Sanic(__name__)
CORS(app)

config = {
    # 'host': '127.0.0.1',
    'host': '0.0.0.0',
    'port': 1337,
    'userdict_path': './wiki_entries.txt',
}

@app.route("/", methods=['POST', 'GET'])
async def index(request):
    """Check if website is accessible"""
    if request.method == 'POST':
        data = request.json
        if data.get('text'):
            text = data['text']
            text = terms_keyword_replace(text)
            return response.json({'text': text})
    return response.json({'text': 'Hello, World!'})

@app.route("/cut", methods=['POST', 'GET'])
async def cut(request):
    replaced = terms_keyword_replace(request.body.decode("utf8"))
    # print(replaced)
    return response.text( replaced )

@app.route("/cut_all", methods=['POST', 'GET'])
async def cut_all(request):
    text_chunks = request.json
    t = Timer()
    t.start()
    replaced = [terms_keyword_replace(x) for x in text_chunks]
    t.stop()
    print(f'本次分词替换, 字串总长度(工作量): {sum(len(s) for s in text_chunks)} 耗时: {round(t.last, 3)} 秒')
    print(replaced[:2])
    return response.json( replaced )

# import cjieba as jieba
import jieba
terms = []
def load_userdict(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            word = line.strip()
            if word:
                jieba.add_word(word) # .add_user_word() if cjieba
                terms.append(word)
load_userdict(config["userdict_path"])

stop_words = rq .get("https://github.com/goto456/stopwords/raw/master/cn_stopwords.txt").text.split('\n')
def terms_keyword_replace(txt):
    words = [ x for x in jieba.cut(txt) ]
    for i in range(len(words)):
        if words[i] in stop_words: continue
        if len(words[i])>1 and words[i] in terms:
            words[i] = f'<span class="u-wiki-entry" data="{words[i]}">{words[i]}</span>'
    return ''.join(words)



if __name__ == '__main__':
    app.run(host=config["host"], port=config["port"], debug=False)