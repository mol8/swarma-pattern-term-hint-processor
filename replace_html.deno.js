import {
  DOMParser,
  initParser,
} from "https://deno.land/x/deno_dom/deno-dom-wasm-noinit.ts";

// type config.*.from = "url" | "localfile";
const config = {
  "wikiEntries": {
    "from": "localfile",
    "location": "./wiki_entries.txt",
  },
  "rawHtml": {
    "from": "url",
    "location": "https://www.dpaste.org/L0VTt/raw",
  },
  // "rawHtml": {
  //   "from": "localfile",
  //   "location": "./test.html",
  // },
  "outputHtmlPath": "./replaced.html",
  // "stopwordsUrl" : "https://github.com/goto456/stopwords/raw/master/cn_stopwords.txt",
  "jiebaServerHost": "http://127.0.0.1:1337",
};

/*
  * get text data from url or local file, depending on config.*.from
  * @returns {Promise<string>}
* */
async function getTextData(source) {
  let textData;
  switch (source.from) {
    case "url":
      const resp = await fetch(source.location);
      textData = await resp.text();
      break;
    case "localfile":
      textData = await Deno.readTextFile(source.location);
      break;
    default:
      break;
  }
  return textData;
}

const html = await getTextData(config.rawHtml);

const domOperate = async (htmlString) => {
  await initParser();
  const doc = new DOMParser().parseFromString(
    htmlString,
    "text/html",
  );

  for (let p of doc.querySelectorAll("p")) { //.forEach(async (p) =>{
    for (let node of p.childNodes) {
      if (!node) continue;
      if (node.nodeName === "#text") {
        const text = node.nodeValue;
        const replacedText = await jieba_wiki_replace(text);
        // console.log(replaced);
        // node.nodeValue = replaced;
        const parentNode = node.parentNode;
        const newDoc = new DOMParser().parseFromString(
          replacedText,
          "text/html",
        );
        const newNodes = newDoc.body.childNodes;
        for (const newNode of newNodes) {
          if (!newNode) continue;
          parentNode.insertBefore(newNode.cloneNode(true), node);
          /* Debug
            console.log("iter", parentNode.innerHTML);
            if (newNode.innerHTML){
              console.log(newNode.outerHTML);
            }else{
              // console.log("iter", parentNode.innerHTML);
              console.log(newNode.textContent);
            }
            */
        }
        parentNode.removeChild(node);
        // console.log(p.innerHTML.length);

      }

    }
  } //)
  return doc;
};
const doc = await domOperate(html);
Deno.writeTextFileSync(config.outputHtmlPath, doc.body.innerHTML);
// await Deno.writeTextFile(config.outputHtmlPath, doc.body.innerHTML);
console.log("html done");

async function jieba_wiki_replace(text) {
  const url = `${config.jiebaServerHost}/cut`;
  const response = await fetch(url, { method: "POST", body: text });
  return await response.text();
}

function deno_native_jieba_wiki_replace() {
  // cannot use jieba for now

  // import { cut, loadDict } from "https://deno.land/x/jieba@v1.0.0/mod.ts";
  // import . from "https://pulipulichen.github.io/jieba-js/require-jieba-js.js";
  // import { python } from "https://deno.land/x/python@0.1.4/mod.ts";
  // const jieba = python.import("jieba");
  // await loadDict(wikiEntriesPath);
  // import * as jieba from "./jieba-wasm/jieba_rs_wasm.js";

  /* terms: keywords from wiki for html content replacing */
  // let terms = await getTextData(config.wikiEntries);
  // terms = terms.split("\n");
  // terms.forEach( word => jieba.add_word(word) ) // jieba.user_dict
  /*
    let stop_words = await getTextData(config.stopwordsUrl)
    stop_words = stop_words.split("\n");

    let words; call_jieba_cut(text, _custom_dict, result => words=result );
    const replaced = words.filter((word) => {
    return !stop_words.includes(word);
    }).map((word) => {
        if (word.length > 1 && wiki.includes(word)) {
            word = `<span class="u-wiki-entry" data="${word}">{${word}</span>`;
        }
    }).join("");
    */
}
